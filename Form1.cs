﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Point nextLocation = new Point(, picAce.location.Y + picAce.size.Height + 10);

            for(int i = 0; i < 6; i++)
            {
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i;
                currentPic.Image = global:WindowsFormsApp1.properties.Resources.ace_black;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    string currentIndex = currentPic.Name.Substring(currentPic.Name.Length - 1);
                    MessageBox.Show("You have clicked card #" + currentIndex);

                    ((PictureBox)sender1).Hide();
                    ((PictureBox)sender1).Dispose();
                };

                this.Controls.Add(currentPic);

                nextLocation.X += currentPic.Size.Width + 10;
                if(nextLocation.X > currentPic.Size.Width)
                {
                    nextLocation.X = Form1_load.Location.X;
                    nextLocation.Y += currentPic.Size.Height + 10;
                }
            }
        }
    }
}
